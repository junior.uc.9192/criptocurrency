import React from 'react';
import { FiMinimize2, FiCheck, FiCloud, FiBriefcase } from "react-icons/fi";
import { GoShield } from "react-icons/go";
import { MdPieChart } from "react-icons/md";
import { FaHandPaper } from "react-icons/fa";

const FILTERS_BUTTONS = [
    {
        name: "payments",
        code: "payments",
        slug: "payments", 
        icon: <FiMinimize2 size="1.2em" className="bg-primary text-white mr-3 mr-md-1" />
    },
    {
        name: "cyber security",
        code: "cyber_security",
        slug: "cyber-security", 
        icon: <GoShield size="1.2em" className="bg-primary text-white mr-3 mr-md-1" />
    },
    {
        name: "forecasting",
        code: "forecasting",
        slug: "forecasting", 
        icon: <MdPieChart size="1.2em" className="bg-primary text-white mr-3 mr-md-1" />
    },
    {
        name: "insurance",
        code: "insurance",
        slug: "insurance",
        default: true, 
        icon: <FiCheck size="1.2em" className="bg-primary text-white mr-3 mr-md-1" />
    },
    {
        name: "cloud storage",
        code: "cloud_storage",
        slug: "cloud-storage", 
        icon: <FiCloud size="1.2em" className="bg-primary text-white mr-3 mr-md-1" />
    },
    {
        name: "charity",
        code: "charity",
        slug: "charity", 
        icon: <FaHandPaper size="1.2em" className="bg-primary text-white mr-3 mr-md-1" />
    },
    {
        name: "transport",
        code: "transport",
        slug: "transport", 
        icon: <FiBriefcase size="1.2em" className="bg-primary text-white mr-3 mr-md-1" />
    }
];

export default FILTERS_BUTTONS;