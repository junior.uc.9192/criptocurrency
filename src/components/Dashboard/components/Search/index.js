import React from 'react';
import { FiSearch } from "react-icons/fi";
import ReactQueryParams from 'react-query-params';
import classNames from 'classnames';
import FILTERS_BUTTONS from 'components/Dashboard/components/Search/constants';
import './styles/Filter.scss';

class Filter extends ReactQueryParams {
    state = {
        search: '',
        tags: ''
    } 

    defaultQueryParams = {
        filter: this.queryParams.filter 
            ? this.queryParams.filter 
            : 'insurance'
    }

    handleFilterChange = event => {
        switch(event.target.id) {
            case "search":
                this.setState({search: event.target.value});
            break;
            default:
                this.setState({tags: event.target.value});
            break;
        }
    }

    render() {
        const {search, tags} = this.state;
        const {filter} = this.queryParams;

        return (
            <section className="container filter p-4">
                <div className="d-flex align-items-center mb-4 p-2 rounded border-bottom bg-light filter__search">
                    <input
                        className="form-control mr-3 filter__search__input" 
                        id="search"
                        name="search"
                        value={search}
                        placeholder="Search Crypto Currencies..."
                        onChange={this.handleFilterChange} />
                    <button className="btn btn-primary p-3 font-weight-bold" type="button">
                        <FiSearch 
                            size="1.2em"
                            className="font-weight-bold text-white" />
                    </button>
                </div>
                <small className="font-weight-bold text-black-50 text-capitalize">Sort by industry:</small>
                <div className="d-flex flex-wrap flex-column flex-md-row justify-content-between filter__tags">
                    {
                        FILTERS_BUTTONS.map((item, index) => (
                            <a 
                                className={classNames({
                                    "btn btn-link text-uppercase font-weight-bold text-left": true,
                                    'active': item.default 
                                        ? filter === undefined || filter === item.slug 
                                        : filter === item.slug 
                                })}
                                href={`?filter=${item.slug}`}
                                key={`filter-${item.slug}`}>
                                    {item.icon}{item.name}
                            </a> 
                        ))
                    }
                    <div>
                        <input 
                            className="form-control form-inline filter__tags__input border-bottom text-center"
                            id="tags"
                            name="tags"
                            value={tags}
                            placeholder="CUSTOMIZE TAGS"
                            onChange={this.handleFilterChange} />
                    </div>
                </div>
            </section>
        );
    }
}

export default Filter;