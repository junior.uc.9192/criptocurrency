const CURRENCIES = [
    {
        logo: "images/cc-btc.svg",
        name: "Bitcoin",
        description: "It is a long established fact that a reader will be distracted by the readable content of a page when looging at its layout.",
        marketCap: "$157,944,929.52",
        price: "$9,379.87",
        circulatingSupply: "16,838,712 BTC",
        volume: "$7,890,100",
        change: -5.36,
        priceGraph: "images/cc-graph2.gif"
    },
    {
        logo: "images/cc-eth.png",
        name: "Ethereum",
        description: "It is a long established fact that a reader will be distracted by the readable content of a page when looging at its layout.",
        marketCap: "$110,572,753,362",
        price: "$1,135.89",
        circulatingSupply: "97,344,596 ETH",
        volume: "$4,199,090",
        change: 12.36,
        priceGraph: "images/cc-graph1.gif"
    },
    {
        logo: "images/cc-ada.png",
        name: "Cardano",
        description: "It is a long established fact that a reader will be distracted by the readable content of a page when looging at its layout.",
        marketCap: "$11,437,519,751",
        price: "$0.441142",
        circulatingSupply: "25,927,070 ADA",
        volume: "$463,719",
        change: "-12.96",
        priceGraph: "images/cc-graph2.gif",
        start: true
    },
    {
        logo: "images/cc-xlm.png",
        name: "Stellar",
        description: "It is a long established fact that a reader will be distracted by the readable content of a page when looging at its layout.",
        marketCap: "$8,702,033,368",
        price: "$0.472171",
        circulatingSupply: "18,429,834 XLM",
        volume: "$165,630",
        change: "-12.96",
        priceGraph: "images/cc-graph1.gif",
        start: true
    }
];

export default CURRENCIES;