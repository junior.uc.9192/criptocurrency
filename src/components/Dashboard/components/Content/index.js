import React from 'react';
import ItemCard from 'components/Dashboard/components/Content/components/ItemCard';
import ItemList from 'components/Dashboard/components/Content/components/ItemList';
import ItemFilter from 'components/Dashboard/components/Content/components/ItemFilter';

class Content extends React.PureComponent {
    render() {
        return (
            <div className="border-top border-bottom container-fluid bg-light pt-4 pb-5">
                <div className="container">
                    <ItemFilter />
                    {/*  item card size <= md */}
                    <div className="d-block d-lg-none">
                        <ItemCard />
                    </div> 
                    {/*  item list size >= lg */}
                    <div className="d-none d-lg-block">
                        <ItemList />
                    </div>
                    <ItemFilter />
                </div>
            </div>
        )
    }
}

export default Content;