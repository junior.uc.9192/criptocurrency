import React from 'react';
import CURRENCIES from 'components/Dashboard/components/Content/constants/index';
import { MdStars } from "react-icons/md";

class ItemCard extends React.PureComponent {
    render() {
        return (
            <>
                {
                    CURRENCIES.map((item, index) => (
                        <div key={`currency-${index}`} className="card mt-3 mb-3">
                            <div className="card-body">
                                <h5 className="card-title font-weight-bold">
                                    <img width="30" src={item.logo} alt={item.name} />&nbsp;{item.name}
                                </h5>
                                <p className="card-text text-justify">
                                    <small className="font-weight-bold text-black-50">{item.description}</small>&nbsp;<small className="font-weight-bold"><a href="/">Read more...</a></small> 
                                </p>
                                <div className="d-flex flex-wrap justify-content-between">
                                    <div className="text-capitalize">
                                        <small className="font-weight-bold text-black-50 text-capitalize">Market cap:</small> {item.marketCap}
                                    </div>
                                    <div className="text-capitalize">
                                        <small className="font-weight-bold text-black-50 text-capitalize">Price:</small> {item.price}
                                    </div>
                                    <div className="text-capitalize">
                                        <small className="font-weight-bold text-black-50 text-capitalize">Circulating supply:</small> {item.circulatingSupply}
                                        {
                                            item.start && (
                                                <sup>&nbsp;<MdStars className="text-info" size="1em"/></sup>
                                            )
                                        }
                                    </div>
                                    <div className="text-capitalize">
                                        <small className="font-weight-bold text-black-50 text-capitalize">volume:</small> {item.volume}
                                    </div>
                                    <div className="text-capitalize">
                                        <small className="font-weight-bold text-black-50 text-capitalize">change (24h):</small>
                                        {
                                            item.change < 0 ? (
                                                <span className="text-danger">&nbsp;{item.change}%</span>
                                            ) : (
                                                <span className="text-success">&nbsp;+{item.change}%</span>
                                            )
                                         } 
                                    </div>
                                </div>
                                <div className="d-flex flex-wrap justify-content-between mt-3 font-weight-bold text-primary">
                                    <a 
                                        className="text-uppercase mr-2" 
                                        href="/" 
                                        target="_blank">website</a>
                                    <a 
                                        className="text-uppercase mr-2" 
                                        href="/"
                                        target="_blank">white paper</a>
                                    <a 
                                        className="text-uppercase" 
                                        href="/"
                                        target="_blank">reddit</a>
                                </div>
                            </div>
                            <div className="card-header text-center">
                                <a
                                    className="d-block"
                                    data-toggle="collapse" 
                                    href={`#collapse${item.name}`} 
                                    role="button" 
                                    aria-expanded="false" 
                                    aria-controls={`collapse${item.name}`}>
                                        <small className="text-capitalize font-weight-bold">Ver gráfico de precio</small>
                                </a>
                                <div className="collapse mt-3" id={`collapse${item.name}`}>
                                    <div className="card card-body">
                                        <img className="img-fluid" src={item.priceGraph} alt={item.name} />
                                        <small className="text-capitalize font-italic">price graph</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))
                }
            </>
        );
    }
}

export default ItemCard;