import React from 'react';
import CURRENCIES from 'components/Dashboard/components/Content/constants/index';
import { MdStars } from "react-icons/md";

class ItemList extends React.PureComponent {
    render() {
        return (
            <div className="mt-2 mt-md-4 table-responsive">
                <table className="table">
                    <caption className="font-italic">
                        <small>List of currencies</small>
                    </caption>
                    <thead>
                        <tr className="font-weight-bold text-black-50 text-capitalize">
                            <th scope="col"><small className="font-weight-bold">#</small></th>
                            <th scope="col" width="300"><small className="font-weight-bold">name and description</small></th>
                            <th scope="col"><small className="font-weight-bold">market cap</small></th>
                            <th scope="col"><small className="font-weight-bold">price</small></th>
                            <th scope="col"><small className="font-weight-bold">circulating supply</small></th>
                            <th scope="col"><small className="font-weight-bold">volume</small></th>
                            <th scope="col"><small className="font-weight-bold">change (24h)</small></th>
                            <th scope="col"><small className="font-weight-bold">price graph</small></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            CURRENCIES.map((item, index) => (
                                <tr className="shadow-sm" key={`currency-${item.name}`}>
                                    <th scope="row">{index+1}.</th>
                                    <td>
                                        <h3>
                                            <img width="30" src={item.logo} alt={item.name} />
                                            &nbsp; {item.name}
                                        </h3>
                                        <small>
                                            <span className="font-weight-bold text-black-50">{item.description}</span> <a className="font-weight-bold" href="/">Read more...</a>
                                            <div className="d-flex mt-3 font-weight-bold text-primary">
                                                <a 
                                                    className="text-uppercase mr-2" 
                                                    href="/" 
                                                    target="_blank">website</a>
                                                <a 
                                                    className="text-uppercase mr-2" 
                                                    href="/"
                                                    target="_blank">white paper</a>
                                                <a 
                                                    className="text-uppercase" 
                                                    href="/"
                                                    target="_blank">reddit</a>
                                            </div>
                                        </small>
                                    </td>
                                    <td>
                                        {item.marketCap}
                                    </td>
                                    <td>
                                        {item.price}
                                    </td>
                                    <td>
                                        {item.circulatingSupply}
                                        {
                                            item.start && (
                                                <sup>&nbsp;<MdStars className="text-info" size="1em"/></sup>
                                            )
                                        }
                                        
                                    </td>
                                    <td>
                                        {item.volume}
                                    </td>
                                    <td>
                                        {
                                            item.change < 0 ? (
                                                <span className="text-danger font-weight-bold">{item.change}%</span>
                                            ) : (
                                                <span className="text-success font-weight-bold">+{item.change}%</span>
                                            )
                                        }
                                    </td>
                                    <td>
                                        <img width="150" height="150" src={item.priceGraph} alt={item.name} />
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

export default ItemList;