import React from 'react';
import { FaLongArrowAltRight } from "react-icons/fa";

class ItemFilter extends React.PureComponent {
    render() {
        return (
            <div className="d-flex flex-column flex-md-row">
                <div className="d-inline-flex mb-sm-2 justify-content-between">
                        <select className="form-control d-inline-block flex-grow-1 mr-2 text-uppercase font-weight-bold">
                            <option>All</option>
                            <option>option 2</option>
                            <option>option 3</option>
                        </select>
                        <select className="form-control d-inline-block flex-grow-1 mr-2 text-uppercase font-weight-bold">
                            <option>coins</option>
                            <option>ethereum</option>
                            <option>cardano</option>
                            <option>stellar</option>
                        </select>
                        <select className="form-control d-inline-block flex-grow-1 mr-2 text-uppercase font-weight-bold">
                            <option>tokens</option>
                            <option>option 2</option>
                            <option>option 3</option>
                        </select>
                </div>
                <div className="ml-md-auto d-inline-flex justify-content-between">
                    <button type="button" className="btn btn-light d-inline-block flex-grow-1 mr-2">
                        Next 100 <FaLongArrowAltRight />
                    </button>
                    <button type="button" className="btn btn-light d-inline-block flex-grow-1 mr-2 text-capitalize">
                        View all
                    </button>
                </div>
            </div>
        );
    }
}

export default ItemFilter;