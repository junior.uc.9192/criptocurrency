import React from 'react';
import Header from 'components/common/Header';
import Filter from 'components/Dashboard/components/Search';
import Content from 'components/Dashboard/components/Content';
import Footer from 'components/common/Footer';

class Dashboard extends React.PureComponent {
    render() {
        return (
            <>
                <Header />
                <Filter />
                <Content />
                <Footer />
            </>
        );
    }
}

export default Dashboard;