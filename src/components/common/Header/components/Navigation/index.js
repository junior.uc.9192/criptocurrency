import React from 'react';
import ProfilePicture from 'components/common/Header/components/ProfilePicture';

class Navigation extends React.PureComponent {
    render() {
        return (
            <nav className="container-fluid p-3 navbar navbar-expand-md navbar-light border-bottom">
                <a 
                    className="navbar-brand text-lowercase mr-5 font-weight-bold"
                    href="/">
                    <img className="mr-2" src="images/cc-logo.png" alt="logo coinmarketcap" />
                    coinmarketcap
                </a>
                <button 
                    className="navbar-toggler" 
                    type="button" 
                    data-toggle="collapse" 
                    data-target="#navbarNavAltMarkup" 
                    aria-controls="navbarNavAltMarkup" 
                    aria-expanded="false" 
                    aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="d-flex flex-column flex-md-row w-100 align-items-center ">
                        <div className="order-1 order-md-0 navbar-nav text-center text-capitalize text-black-50 font-weight-bold">
                            <a className="nav-item nav-link active" href="/">market cap</a>
                            <a className="nav-item nav-link" href="/">trade volume</a>
                            <a className="nav-item nav-link" href="/">trending</a>
                            <a className="nav-item nav-link" href="/">tools</a>
                        </div>
                        <div className="order-2 order-md-1 ml-md-auto d-flex justify-content-center">
                            <select className="form-control mr-2 text-uppercase font-weight-bold">
                                <option>Eng</option>
                                <option>Spa</option>
                            </select>
                            <select className="form-control text-uppercase font-weight-bold">
                                <option>usd</option>
                                <option>btc</option>
                                <option>eth</option>
                            </select>
                        </div>
                        <div className="d-flex order-0 order-md-2 justify-content-center">
                            <ProfilePicture />
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}

export default Navigation;