import React from 'react';

class ProfilePicture extends React.PureComponent {
    render() {
        return (
            <div className="dropdown">
                <button 
                    className="btn dropdown-toggle" 
                    type="button" 
                    data-toggle="dropdown">
                    <img
                        width="50"
                        className="img-responsive" 
                        src="images/cc-picture.png" 
                        alt="my profile" />
                    <span className="caret"/>
                </button>
                <div className="dropdown-menu dropdown-menu-right text-capitalize" aria-labelledby="dropdownMenuButton">
                    <a className="dropdown-item" href="/">my profile</a>
                    <a className="dropdown-item" href="/">wallet</a>
                    <a className="dropdown-item" href="/">transfer</a>
                </div>
            </div>
        );
    }
}

export default ProfilePicture;