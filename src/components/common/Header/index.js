import React from 'react';
import Navigation from 'components/common/Header/components/Navigation';

class Header extends React.PureComponent {
    render() {
        return (
            <>
                {/*  statistics size >= md */}
                <section className="d-none d-md-block container-fluid border bg-light text-secondary">
                    <div className="d-flex justify-content-between">
                        <div className="text-capitalize">
                            <small className="font-weight-bold text-black-50 mr-3">
                                Criptocurrencies: <span className="text-primary font-weight-bold">1514</span>
                            </small>
                            <small className="font-weight-bold text-black-50">
                                Markets: <span className="text-primary font-weight-bold">8655</span>
                            </small>
                        </div>
                        <div>
                            <small className="font-weight-bold text-black-50 text-capitalize mr-3">
                                market cap: <span className="text-primary font-weight-bold">$403,744,438,209</span>
                            </small>
                            <small className="font-weight-bold text-black-50 text-capitalize mr-3">
                                24h volume: <span className="text-primary font-weight-bold">$22,744438,209</span>
                            </small>
                            <small className="font-weight-bold text-black-50">
                                BTC Dominance: <span className="text-primary font-weight-bold">34.8%</span>
                            </small>
                        </div>
                    </div>
                </section>
                {/*  Navigation bar */}
                <Navigation />
                {/*  statistics size <= sm */}
                <section className="d-block d-md-none container-fluid px-0 text-center">
                    <div id="accordion">
                        <div className="card border-0">
                            <div className="card-header p-0" id="headingOne">
                                    <button className="btn btn-link btn-block mb-0 py-2 font-weight-bold" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Statistics
                                    </button>
                            </div>
                        </div>
                        <div id="collapseOne" className="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <ul className="list-group">
                                <li className="list-group-item">
                                    <small className="font-weight-bold text-black-50 text-capitalize">
                                        Criptocurrencies: <span className="text-primary">1514</span>
                                    </small>
                                </li>
                                <li className="list-group-item">
                                    <small className="font-weight-bold text-black-50 text-capitalize">
                                        Markets: <span className="text-primary">8655</span>
                                    </small>
                                </li>
                                <li className="list-group-item">
                                    <small className="font-weight-bold text-black-50 text-capitalize">
                                        market cap: <span className="text-primary">$403,744,438,209</span>
                                    </small>
                                </li>
                                <li className="list-group-item">
                                    <small className="font-weight-bold text-black-50 text-capitalize">
                                        24h volume: <span className="text-primary">$22,744438,209</span>
                                    </small>
                                </li>
                                <li className="list-group-item">
                                    <small className="font-weight-bold text-black-50">
                                        BTC Dominance: <span className="text-primary">34.8%</span>
                                    </small>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>
            </>
        )
    }
}

export default Header;