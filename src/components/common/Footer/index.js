import React from 'react';

class Footer extends React.PureComponent {
    render() {
        return (
            <footer className="page-footer font-small blue pt-4">
                <div className="container-fluid text-center mt-md-0 mt-3">
                    <h5 className="text-uppercase">
                        <a 
                            className="navbar-brand text-lowercase mr-5 font-weight-bold"
                            href="/">
                            <img width="50" className="mr-2" src="images/cc-logo.png" alt="logo coinmarketcap" />
                            coinmarketcap
                        </a>
                    </h5>
                </div>
                <div className="footer-copyright text-center py-3">© 2019 Copyright:
                    <a href="/"> Todos los derechos reservados por el autor</a>
                </div>
            </footer>
        );
    }
}

export default Footer;